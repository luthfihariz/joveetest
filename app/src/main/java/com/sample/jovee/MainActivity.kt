package com.sample.jovee

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sample.core.visible
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvView.visible()
    }
}
